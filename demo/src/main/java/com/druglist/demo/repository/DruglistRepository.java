package com.druglist.demo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.druglist.demo.model.Druglist;

@Repository
public interface DruglistRepository extends MongoRepository<Druglist, String>{
	public Druglist findBydrugName(String DrugName);
}
