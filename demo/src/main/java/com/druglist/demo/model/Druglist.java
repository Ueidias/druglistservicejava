package com.druglist.demo.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "drug_list")
public class Druglist {
	@Id
	String _id;
	String DrugName;
	String Remarks;

	public String getDrugName() {
		return DrugName;
	}

	public void setDrugName(String DrugName) {
		this.DrugName = DrugName;
	}

	public String getRemarks() {
		return Remarks;
	}

	public void setRemarks(String Remarks) {
		this.Remarks = Remarks;
	}

	public Druglist(String DrugName, String Remarks) {
		this.DrugName = DrugName;
		this.Remarks = Remarks;
	}

}
