package com.druglist.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.druglist.demo.model.Druglist;
import com.druglist.demo.service.DruglistService;

@RestController
public class DruglistController {

	@Autowired
	private DruglistService druglistservice;
	
	@RequestMapping("/create")
	public String create(@RequestParam String DrugName,@RequestParam String Remarks) {
		Druglist dl=druglistservice.create(DrugName, Remarks);
		return dl.toString();
	}
	
	@RequestMapping("/getAll")
	public List getBook() {
		return druglistservice.getAll();
	}
}
