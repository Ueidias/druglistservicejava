package com.druglist.demo.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.druglist.demo.model.Druglist;
import com.druglist.demo.repository.DruglistRepository;

@Service
public class DruglistService {

	@Autowired
	private DruglistRepository druglistrepository;
	
	public Druglist create(String DrugName,String Remarks) {
		return druglistrepository.save(new Druglist(DrugName,Remarks));
	}

	public List<Druglist>getAll(){
		return druglistrepository.findAll();
	}
}
